﻿// -----------------------------------------------------------------------
// <copyright file="BinaryCode.cs" company="Sector84">
//     Copyright (c) Sector84 Entertainment. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace BinaryCode_Decode
{
    using System;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// <para>
    /// Class definition for BinaryCode.cs
    /// </para>
    /// <para>
    /// SRM 144 DIV 1 (300 points)
    /// </para>
    /// <para>
    /// Problem Statement 
    /// Let's say you have a binary string such as the following:
    /// 011100011
    /// One way to encrypt this string is to add to each digit the sum of its adjacent digits. For example, the above string would become:
    /// 123210122
    /// In particular, if P is the original string, and Q is the encrypted string, then Q[i] = P[i-1] + P[i] + P[i+1] for all digit positions i. Characters off the left and right edges of the string are treated as zeroes.
    /// An encrypted string given to you in this format can be decoded as follows (using 123210122 as an example):
    /// Assume P[0] = 0.
    /// Because Q[0] = P[0] + P[1] = 0 + P[1] = 1, we know that P[1] = 1.
    /// Because Q[1] = P[0] + P[1] + P[2] = 0 + 1 + P[2] = 2, we know that P[2] = 1.
    /// Because Q[2] = P[1] + P[2] + P[3] = 1 + 1 + P[3] = 3, we know that P[3] = 1.
    /// Repeating these steps gives us P[4] = 0, P[5] = 0, P[6] = 0, P[7] = 1, and P[8] = 1.
    /// We check our work by noting that Q[8] = P[7] + P[8] = 1 + 1 = 2. Since this equation works out, we are finished, and we have recovered one possible original string.
    /// Now we repeat the process, assuming the opposite about P[0]:
    /// Assume P[0] = 1.
    /// Because Q[0] = P[0] + P[1] = 1 + P[1] = 1, we know that P[1] = 0.
    /// Because Q[1] = P[0] + P[1] + P[2] = 1 + 0 + P[2] = 2, we know that P[2] = 1.
    /// Now note that Q[2] = P[1] + P[2] + P[3] = 0 + 1 + P[3] = 3, which leads us to the conclusion that P[3] = 2. However, this violates the fact that each character in the original string must be '0' or '1'. Therefore, there exists no such original string P where the first digit is '1'.
    /// Note that this algorithm produces at most two decodings for any given encrypted string. There can never be more than one possible way to decode a string once the first binary digit is set.
    /// Given a string message, containing the encrypted string, return a string[] with exactly two elements. The first element should contain the decrypted string assuming the first character is '0'; the second element should assume the first character is '1'. If one of the tests fails, return the string "NONE" in its place. For the above example, you should return {"011100011", "NONE"}.
    /// </para>
    /// <para>
    /// Definition 
    /// Class:
    /// BinaryCode
    /// Method:
    /// decode
    /// Parameters:
    /// string
    /// Returns:
    /// string[]
    /// Method signature:
    /// string[] decode(string message)
    /// (be sure your method is public)
    /// </para>
    /// <para>
    /// Limits
    /// Time limit (s):
    /// 2.000
    /// Memory limit (MB):
    /// 64
    /// </para>
    /// <para>
    /// Constraints
    /// -message will contain between 1 and 50 characters, inclusive.
    /// -Each character in message will be either '0', '1', '2', or '3'.
    /// </para>
    /// <para>
    /// Examples
    /// 0)
    /// "123210122"
    /// Returns: { "011100011",  "NONE" }
    /// 1)
    /// "11"
    /// Returns: { "01",  "10" }
    /// We know that one of the digits must be '1', and the other must be '0'. We return both cases.
    /// 2)
    /// "22111"
    /// Returns: { "NONE",  "11001" }
    /// Since the first digit of the encrypted string is '2', the first two digits of the original string must be '1'. Our test fails when we try to assume that P[0] = 0.
    /// 3)
    /// "123210120"
    /// Returns: { "NONE",  "NONE" }
    /// This is the same as the first example, but the rightmost digit has been changed to something inconsistent with the rest of the original string. No solutions are possible.
    /// 4)
    /// "3"
    /// Returns: { "NONE",  "NONE" }
    /// 5)
    /// "12221112222221112221111111112221111"
    /// Returns: 
    /// { "01101001101101001101001001001101001",
    ///   "10110010110110010110010010010110010" }
    /// </para>
    /// </summary>
    public class BinaryCode
    {
        /// <summary>
        /// Decodes the given string.
        /// </summary>
        /// <param name="message"></param>
        /// <returns>The result.</returns>
        public string[] Decode(string message)
        {
            // First, assume zero
            string assumeZero = DecodeAssumption(0, message);

            // Next, assume one
            string assumeOne = DecodeAssumption(1, message);

            // Return our results
            return new[] { assumeZero, assumeOne };
        }

        /// <summary>
        /// Decodes the string assuming the first character is either 0 or 1.
        /// </summary>
        /// <param name="assumption"></param>
        /// <param name="message"></param>
        /// <returns>
        /// The decoded string, or "NONE" if the decoding fails.
        /// </returns>
        private string DecodeAssumption(int assumption, string message)
        {
            // Ceate a new integer array the same length as the string
            int[] decryptedValues = new int[message.Length];

            // Our first decrypted value will always be our assumption
            decryptedValues[0] = assumption;

            // It's always possible to calculate the NEXT decrypted value given
            // our current and previous decrypted values and the current encrypted
            // value.
            for (int i = 0; i < message.Length; i++)
            {
                // We already know the decrypted value for our previous index.
                // If we're one the first index, assume the previous value is 0.
                int previousDecrypted = i == 0 ? 0 : decryptedValues[i - 1];

                // We also already know the decrypted value for our current index
                int currentDecrypted = decryptedValues[i];

                // The forumla is as follows:
                // Previous + Current + Next = Sum (encrypted value)
                int sum = previousDecrypted + currentDecrypted;
                int targetSum = int.Parse(message[i].ToString());

                // If we're on the last value, we can't add any other decoded
                // values. We simply need to check to see if the last value
                // is valid.
                if (i == message.Length - 1)
                {
                    if (targetSum != sum)
                        return "NONE";
                }
                else
                {
                    // The only part of the formula we are missing is the decrypted 
                    // value of the NEXT index. The value should either be 0 or 1.
                    // If it is not, our assumption value was incorrect, and the 
                    // algorithm failed.
                    if (targetSum - sum == 0)
                        decryptedValues[i + 1] = 0;
                    else if (targetSum - sum == 1)
                        decryptedValues[i + 1] = 1;
                    else
                        return "NONE";
                }
            }

            // Build a string from our results and return
            StringBuilder sb = new StringBuilder();
            foreach (int i in decryptedValues)
            {
                sb.Append(i);
            }

            return sb.ToString();
        }
    }
}