﻿// -----------------------------------------------------------------------
// <copyright file="Program.cs" company="Sector84">
//     Copyright (c) Sector84 Entertainment. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace BinaryCode_Decode
{
    using System;
    using System.Linq;

    /// <summary>
    /// Class definition for Program.cs
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// Entry point.
        /// </summary>
        /// <param name="args">Entry args.</param>
        public static void Main(string[] args)
        {
            // Execute
            BinaryCode bc = new BinaryCode();
            string[] result = bc.Decode("22111");

            // Test
            foreach (string s in result)
            {
                Console.WriteLine(s);
            }
        } 
    }
}
